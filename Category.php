<?php 

class Category
{

	public static function getCategoriesList()
	{

		$db = Db::getConnection();
		$result = $db->prepare("SELECT id, name FROM category ORDER BY `sort_order` ASC");
		$result->execute();
		$data = $result->fetchAll();
		return $data;
	}
}