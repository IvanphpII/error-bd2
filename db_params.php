<?php 

return array(
	'host' => 'localhost',
	'dbname' => 'phpshop',
	'user' => 'root',
	'password' => 'root',
	'options' => array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC		
		)
	);