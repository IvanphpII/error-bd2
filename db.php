<?php 

class Db
{

	public static function getConnection()
	{

		$dbPath = ROOT . '/config/db_params.php';
		// $params = include_once $dbPath;
		$params = include_once ROOT . '/config/db_params.php';

		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";

		$pdo = new PDO($dsn, $params['user'],$params['password'],$params['options']);

		return $pdo;
	}
}